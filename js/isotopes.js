// external js: isotope.pkgd.js

$(document).ready(function() {
  // init Isotope
  var $container = $('.isotope').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows'
  });

  //****************************
  // Isotope Load more button
  //****************************
  var initShow = 15; //number of items loaded on init & onclick load more button
  var counter = initShow; //counter for load more button
  var iso = $container.data('isotope'); // get Isotope instance

  loadMore(initShow); //execute function onload

  function loadMore(toShow) {
    $container.find('.hidden').removeClass('hidden');

    var hiddenElems = iso.filteredItems
      .slice(toShow, iso.filteredItems.length)
      .map(function(item) {
        return item.element;
      });
    $(hiddenElems).addClass('hidden');
    $container.isotope('layout');

    //when no more to load, hide show more button
    if (hiddenElems.length == 0) {
      jQuery('#load-more').hide();
    } else {
      jQuery('#load-more').show();
    }
  }

  //when load more button clicked
  $('#load-more').click(function() {
    counter = counter + initShow;

    loadMore(counter);
  });
});
