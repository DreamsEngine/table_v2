<?php include('header.php'); ?>
<!-- wrapper starts -->
<div class="wrapper">
   <?php include('inner1.php'); ?>
   <?php include('inner2.php'); ?>
   <?php include('inner3.php'); ?>
   <?php include('inner4.php'); ?>
   <?php include('footer.php'); ?>
</div>

      <!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.dd.js"></script> -->
      <!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripting.js"></script> -->
      <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/customInput.jquery.js"></script>
      <!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script> -->
      <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
      <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/app.js"></script>
      <script src="https://npmcdn.com/isotope-layout@2.2.2/dist/isotope.pkgd.js"></script>
      <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/isotopes.js"></script>
      <script>
         
        
      
         $(document).ready(function(e){
            $('nav').clone().appendTo('.sidemenu');
            $('.menu').click(function(e){
               $('body').toggleClass('open-menu');
         })
      
             $('.foodandwines-links').clone().appendTo('.footer-logo');
             $('.foodandwine-cnt').clone().appendTo('.footer-logo');
             $('.foodandwine-data').clone().appendTo('.footer-bottom');
             $('.copy-right').clone().appendTo('.footer-bottom');
            //  $(".horizontalvertical_scroll").mCustomScrollbar({
            //              axis:"yx",
            //             });
             $( " .grid-block" ).mouseover(function() {

            $('.grid-block.grid-lorem-data').hide();
            });
            $( " .element-item.mobile-content" ).mouseover(function() {
            $('.grid-block.grid-lorem-data').show();
            });
            var items =  22;
            var shown =  11; 
            $('.mylist .kesif-gonderi-alani').fadeOut().hide();
            $('.mylist .kesif-gonderi-alani:lt(11)').fadeIn().show();
    
            $('.loadmore').click(function () {
               shown = $('.mylist .kesif-gonderi-alani:visible').length + 8;
               if(shown< items) {
                  $('.mylist .kesif-gonderi-alani:lt('+shown+')').show().fadeIn();
               //$('.mylist .kesif-gonderi-alani:lt('+shown+')').siblings().removeClass('active');
               } else {
                  $('.mylist .kesif-gonderi-alani:lt('+items+')').show().fadeIn();
                  $('.loadmore').hide();
               }
            });
		
            $( function() {
            
            var $container = $('.posts-holder');
               $container.masonry({
               isFitWidth: true,
               itemSelector: '.kesif-gonderi-alani'
               });  
            });
        

         var ww = $(window).width();
         var limit = 767;
         function refresh() {
         ww = $(window).width();
         var w = ww<limit ? (location.reload(true)) : ( ww>limit ? (location.reload(true)) : ww=limit );
         }
         
         var tOut;
         $(window).resize(function() {
         var resW = $(window).width();
         clearTimeout(tOut);
         if ( (ww>limit && resW<limit) || (ww<limit && resW>limit) ) { 
         tOut = setTimeout(refresh, 100);
         }
         });

         });

         window.onscroll = function() {myFunction()};
         var navbar = document.getElementById("navbar");
         var sticky = navbar.offsetTop;

         function myFunction() {
         if (window.pageYOffset > sticky) {
            navbar.classList.add("sticky")
         } else {
            navbar.classList.remove("sticky");
         }
         }

         function showContent() {
            const element = document.getElementById("content");
            const check = document.getElementById("btn-menu3");
            const open = document.getElementById("open");
            const close = document.getElementById("close");

            if (check.checked) {
                  element.style.display='block';
                  close.style.display='block'
            }
            else {
                  element.style.display='none';
                  close.style.display='none'
            }

    }

    function revertContent() {
      const mmenu = document.querySelectorAll('#content li a');
		const check = document.querySelector('#navbar input');

	   mmenu.forEach(function(item){
         item.addEventListener('click', function(){
            check.checked = false;
            showContent();        
			
    });
});

}


    revertContent();

   
         
      </script>
   </body>
</html>