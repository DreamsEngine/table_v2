<?php 
add_theme_support( 'post-thumbnails' ); 
add_filter( 'rwmb_meta_boxes', function ( $meta_boxes ) {
    $meta_boxes[] = [
        'title'  => 'Caracteristicas',
        'fields' => [
            [
                'id'   => 'id_day',
                'name' => 'Día',
                'type' => 'select',
                'options' => array(
                    '01' => '01',
                    '02' => '02',
                    '03' => '03',
                    '04' => '04',
                    '05' => '05',
                    '06' => '06',
                    '07' => '07',
                    '08' => '08',
                    '09' => '09',
                    '10' => '10',
                    '11' => '11',
                    '12' => '12',
                    '13' => '13',
                    '14' => '14',
                    '15' => '15',
                    '16' => '16',
                    '17' => '17',
                    '18' => '18',
                    '19' => '19',
                    '20' => '20',
                    '21' => '21',
                    '22' => '22',
                    '23' => '23',
                    '24' => '24',
                    '25' => '25',
                    '26' => '26',
                    '27' => '27',
                    '28' => '28',
                    '29' => '29',
                    '30' => '30',
                    '31' => '31',
                ),
            ],
            [
                'id' => 'id_month',
                'name' => 'Mes',
                'type' => 'select',
                'options' => array(
                    'Enero' => 'Enero',
                    'Febrero' => 'Febrero',
                    'Marzo' => 'Marzo',
                    'Abril' => 'Abril',
                    'Mayo' => 'Mayo',
                    'Junio' => 'Junio',
                    'Julio' => 'Julio',
                    'Agosto' => 'Agosto',
                    'Septiembre' => 'Septiembre',
                    'Octubre' => 'Octubre',
                    'Noviembre' => 'Noviembre',
                    'Diciembre' => 'Diciembre',
                )
            ],
            [
                'id'      => 'id_name',
                'name'    => 'Nombre del Experiencia',
                'type'    => 'text'
            ],
            [
                'id'   => 'id_desc',
                'name' => 'Descripción del evento',
                'rows' => '8',
                'type' => 'textarea'
            ],
            [
                'id'   => 'id_boletos',
                'name' => 'Link para boletos',
                'type' => 'text'
            ],
            [
                'id' => 'id_disponible',
                'name' => 'Boletos disponibles/evento vigente',
                'type' => 'radio',
                'options' => array (
                    '0' => 'No',
                    '1' => 'Si'
                )
            ],
            [
                'id' => 'id_letrero',
                'name' => 'Agregar letrero?',
                'type' => 'radio',
                'options' => array (
                    '0' => 'No',
                    '1' => 'Si'
                )
            ],
        ],
    ];
    
    return $meta_boxes;
});

function fecha_dia(){
    $dia = rwmb_meta( 'id_day' );
    echo $dia;
}

function fecha_mes(){
    $mes = rwmb_meta( 'id_month' );
    echo $mes;
}
function nombre_exp(){
    $nombre = rwmb_meta( 'id_name' );
    echo $nombre;
}
function exp_desc(){
    $desc = rwmb_meta( 'id_desc' );
    echo $desc;
}
function url_boletos(){
    $boleto = rwmb_meta( 'id_boletos' );
    echo $boleto;
}
function disponibilidad(){
    $dis = rwmb_meta('id_disponible');
    return $dis;
}
?>