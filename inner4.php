<div class="banner3  vcard" id="sede" itemscope itemtype="https://schema.org/banner2">
            <div class="banner3-content">
            <div class="banner3-img1"></div>
            <div>
               <h3>Hotel Sede</h3>
               <p class="text-banner3">Pug Seal</p>
               <p>Este boutique bed & breakfast hace que todos los chefs que nos visitan se sientan como en casa. Cada una de sus habitaciones cuenta una historía única a través de su decoración ecléctica pero bien pensada. El servicio personalizado y calidez que los caracteriza, es capaz de conquistar incluso a los expertos en el tema de hospitalidad. 
               </p>
               <p class="hotel-sede-btn"> <a href="https://pugseal.com/#firstPage" target="_blank" class="btn" itemprop="url">Conoce más</a></p>
               </div>
               <div class="banner3-img2"></div>
               </div>
   
</div>
