         <!-- calendario-content starts -->
         <div class="calendario-content vcard" id="calendar" itemscope itemtype="https://schema.org/calendario">
            <div class="inner-wraps">
               <h1>Experiencias</h1>

               <!-- calendario-content ends -->



               <!--=====================================   SLICK   ===********************************************============================-->


               <div class="calendar-cont">
                  <div class="" data-glide-el="track">
                     <div class="slide_arrows">
                        <button id="arrow_left" class="glide__arrow glide__arrow--left" data-glide-dir="<"><i class="fas fa-chevron-left"></i></button>
                        <figure>
                        <img class="swipe-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/swipe.png"></figure>
                        <button id="arrow_right" class="glide__arrow glide__arrow--left" data-glide-dir="<"><i class="fas fa-chevron-right"></i></button>
                     </div>
                     <div class="">
                        <div class="enero-link-main clearfix exp_carousel">
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Febrero',
                              'order' => 'ASC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Marzo',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>
                                             

                                          </div>
                                          <div class="enrique-olvera">
                                          <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Abril',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>

                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Mayo',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                             
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Junio',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                             
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Julio',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>

                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Agosto',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>
                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                             
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Septiembre',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>
                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Octubre',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>
                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                             
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?> <?php
                              $args = array(
                                 'post_type' => 'post',
                                 'meta_key' => 'id_month',
                                 'meta_value' => 'Noviembre',
                                 'order' => 'DESC'
                              );
                              $query = new WP_Query($args);
                              if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                    $thumbnail = get_the_post_thumbnail_url();
                              ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>
                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                                 endwhile;
                              endif;
                              wp_reset_postdata();
                           ?>
                           <?php
                           $args = array(
                              'post_type' => 'post',
                              'meta_key' => 'id_month',
                              'meta_value' => 'Diciembre',
                              'order' => 'DESC'
                           );
                           $query = new WP_Query($args);
                           if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                                 $thumbnail = get_the_post_thumbnail_url();
                           ?>
                                 <div>
                                    <a href="#" class="enero-btn u-url" itemprop="url"><?php fecha_mes(); ?></a>
                                    <div class="enero-links">
                                       <div class="enero-links-top">
                                          <div class="quedan">
                                             <span><?php fecha_dia(); ?></span>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares u-url" itemprop="url"><?php the_title(); ?></a>
                                             <a href="<?php echo the_permalink(); ?>" class="quedan-lugares-movil u-url" itemprop="url"><?php the_title(); ?></a>

                                          </div>
                                          <div class="enrique-olvera">
                                             <p class="enrique-olvera-para-movil"><?php exp_desc(); ?></p>

                                             <p class="enrique-olvera-para"><?php exp_desc(); ?></p>
                                          </div>
                                       </div>
                                       <div class="enero-links-bottom">
                                          <a href="<?php url_boletos(); ?>" target="_blank" class="btn--sell">Compra boletos</a>
                                       </div>
                                    </div>
                                 </div>
                           <?php
                              endwhile;
                           endif;
                           wp_reset_postdata();
                           ?>
                        </div>
                     </div>
                  </div>
               </div>

               <!--=====================================   ACABA SLICK   ===============================-->

               <!-- wrapper ends -->
               <!-- SET: SCRIPTS -->
               <!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.2.min.js"></script> -->
               <!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripting.js"></script> -->
               <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/customInput.jquery.js"></script>
               <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
               <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
               <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/glide.js"></script>


               <script>
                  /*  var glide = new Glide('.glide', {
               startAt: 0,
               type: 'slider', //hay dos tipos slider y carrousel
               perView: 1, //cantidad de slide en pantalla
               breakpoints: { //para el tamaño de pantalla
                  1024: {
                     perView: 1,
                     peek: {
                        before: 1,
                        after: 200
                     }
                  },
                  991: {
                     perView: 1,
                     peek: {
                        before: 1,
                        after: 90
                     }
                  },

                  414: {
                     perView: 1,
                     peek: {
                        before: -100,
                        after: 40
                     }
                  },
               },
               peek: {
                  before: 0,
                  after: 350
               }
            })
            glide.mount(); */

                  // $(document).ready(function(e){
                  //     $(".horizontalvertical_scroll").mCustomScrollbar({
                  //                 axis:"yx",

                  // });

                  // var ww = $(window).width();
                  // var limit = 767;
                  // function refresh() {
                  // ww = $(window).width();
                  // var w = ww<limit ? (location.reload(true)) : ( ww>limit ? (location.reload(true)) : ww=limit );
                  // }

                  // var tOut;
                  // $(window).resize(function() {
                  // var resW = $(window).width();
                  // clearTimeout(tOut);
                  // if ( (ww>limit && resW<limit) || (ww<limit && resW>limit) ) { 
                  // tOut = setTimeout(refresh, 100);
                  // }
                  // });


                  // });
               </script>
               </body>

               </html>