<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/cheff/contacto.css">
<footer id="footer">
   <div class="container">
      <div class="sponsors">
         <ul>
            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Nespresso.png" alt="Nespresso">
            </li>
            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Glenlivet.png" alt="Glenlivet">
            </li>

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/logo-miele.png" alt="Miele">
            </li>

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/IteramericanaW.png" alt="Interamericana">
            </li>

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Ferrer.png" alt="Ferrer">
            </li>

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Riedel.png" alt="Riedel">
            </li>

            <!-- <li>
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Logo -puseal.jpg" alt="Pugseal">
                  </li> -->

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/focal.png" alt="Focal">
            </li>

            <!-- <li>
                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Logo _tea.jpg" alt="Tea Forte">
                  </li> -->

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/Evian.png" alt="Evian">
            </li>

            <li>
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sponsors/badoitpng.png" alt="Badoit">
            </li>
         </ul>
      </div>


      <div class="clear"></div>
      <div class="table-contact-main" id="directorio">
         <div class="table-chefs-left table-contacto-left flt_lt directorio">
            <h3><span></span>Directorio</h3>

            <h2>Directora editorial Food & Wine en español</h2>

            <ul contact_left_ul>
               <li>
                  <p class="contact_left_p">- Mariana Camacho <span> (mcamacho@foodandwine.mx)</span></p>
               </li>
            </ul>

            <br>
            <h2>Directora Comercial de Eventos</h2>

            <ul>
               <li>
                  <p class="contact_left_p">- Elisa Cadenas <span> (ecadenas@forbes.com.mx)</span></p>
               </li>
            </ul>
            <br>
            <h2>Marketing y Relaciones Públicas </h2>

            <ul>
               <li>
                  <p class="contact_left_p">- Mar&iacute;a Escriv&aacute; de Balaguer <span> (mescriva@foodandwine.mx) <span></p>
               </li>
            </ul>

         </div>
         <div class="table-chefs-right table-contacto-right flt_lt">
            <h3>
               ¿Quieres hacer un evento en Food & Table?
            </h3>
            <h2> Déjanos tus datos y nos pondremos en contacto</h2>
            <?php echo do_shortcode('[wpforms id="9"]'); ?>

            <!-- <form class="form-contacto" action="" method="get">
                           <h2> Déjanos tus datos y nos pondremos en contacto</h2>
                           <div class="">
                              <input class="form-contact-input" style="font-size:20px;" type="text" id="name" size="40" maxlength="100" name="nombre" placeholder="  Nombre">
                           </div>
                           <div class="form-contact-div">
                              <input class="form-contact-input" style="font-size:20px;" type="email" id="mail" size="40"  maxlength="60" name="nombre" placeholder="  Correo">
                           </div>
                           <div class="form-contact-divArea">
                              <textarea class="form-contact-text" style="font-size:20px;" name="comentarios" rows="10" cols="39" placeholder="  Comentarios"></textarea>
                           </div>
                           <div class="">
                              <button style="width:100px; height:50px; background-color: transparent; color:#bbbc7a; border: solid 1px #bbbc7a; font-size:20px;" type="submit">Enviar</button>
                           </div>
                        </form> -->
         </div>
         <div class="clear">&nbsp;</div>
      </div>

      <hr>
      <div class="footer-main">
         <div class="footer-logo flt_lt">
            <a href="#">
               <amp-img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-table-amex-wh.svg" width="300" height="170" alt="footer_logo"></amp-img>
            </a>
         </div>
         <div class="footer-right flt_rt">
            <ul class="footer-links clearfix">
               <li><a href="mailto:able@foodandwine.mx">Contáctanos</a></li>
               <li><a href="mailto:ecadena@forbes.com.mx">Ventas</a></li>
               <li><a href="mailto:mescriva@forbes.com.mx">Marketing y Relaciones Públicas</a></li>
            </ul>
            <div class="president-masary flr_rt">
               <ul class="foodandwines-links clearfix flt_rt">
                  <li>
                     <p class="light">Edgar Allan Poe 42, Polanco, Miguel Hidalgo, CDMX </p>
                  </li>
                  <li>
                     <a href="https://foodandwineespanol.com/" class="foodandwine-cnt">foodandwineespanol.com</a>

                  </li>
                  <li>
                     <a href="#">
                        <i class="fab fa-twitter"></i>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <i class="fab fa-facebook-f"></i>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <i class="fab fa-instagram"></i>
                     </a>
                  </li>
                  <li><a href="#">&#64;FoodandWineEs</a></li>

               </ul>
            </div>



            <p class="copy-right">&copy; <?php
                                          $fromYear = 2018;
                                          $thisYear = (int) date('Y');
                                          echo $fromYear . (($fromYear != $thisYear) ? '-' . $thisYear : ''); ?> Food & Wine México.</p>
         </div>
         <div class="clear"></div>
      </div>
   </div>

   <!--==============     SCRIPTS    ================-->
   <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick/slick.min.js"></script>

   <script>
      $(document).ready(function() {
        
         $('.exp_carousel').slick({
            autoplay: false,
            slidesToShow: 2,
            infinite:false,
            arrows: false,
            dots: false,
            // the magic
  responsive: [{

breakpoint: 768,
settings: {
  slidesToShow: 1,
  dots: false
}

}, {

breakpoint: 300,
settings: "unslick" // destroys slick

}]
         });

         $('#arrow_right').click( function (){
            $('.exp_carousel').slick('slickNext');
         });

         $('#arrow_left').click( function (){
                 $('.exp_carousel').slick('slickPrev');
         });

      });
   </script>
</footer>