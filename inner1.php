         <!-- banner starts -->

         <div class="banner2  vcard" id="about" itemscope itemtype="https://schema.org/banner2">
            <div class="banner2-content">
            
               <h1>¿Qué es Table?</h1>
               <p>Ubicado en el corazón del barrio gastronómico de la Ciudad de México,<span class="text-food"> Food & Wine Table by American Express</span> es un espacio destinado a la cocina y a la colaboración, a la creatividad, a las artes y oficios que rodean a la buena mesa. Mes a mes Food & Wine Table será el anfitrión de experiencias culinarias diseñadas de la mano de los mejores chefs de México y el mundo siempre bajo la curaduría de nuestros expertos editoriales en gastronomía y enología. 
               </p>
               <div class="banner2-content--links">
               <a href="#chefs" class="btn" itemprop="url">Chefs invitados</a> 
               <a href="#calendar" class="btn" itemprop="url">Experiencias</a> 
               <a href="https://www.youtube.com/watch?v=Pkvk1U7_UM8&feature=youtu.be" target="_blank" class="btn" itemprop="url">Conoce el espacio</a>
               </div>
            </div>
         </div>
