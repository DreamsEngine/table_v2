<?php /* Template Name: Contacto */ ?>


<!DOCTYPE html>
<html amp lang="en">
	<head>
		<meta charset="utf-8" />
		<script async src="https://cdn.ampproject.org/v0.js"></script>
		<title>Table - Food and Wine México</title>
		<link rel="canonical" href="https://html1118.upupload.com/67102/tvkroguh67102/inner4.html" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="format-detection" content="telephone=no" />
		<!-- SET: FAVICON -->
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
		<!-- END: FAVICON -->
		<!-- SET: STYLESHEET -->
		<link href="https://fonts.googleapis.com/css?family=Heebo:100,400,500,700" rel="stylesheet" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/cheff/styleCheff.css" />
		<!-- <link rel="stylesheet" href="/style.css" /> -->
		<!-- Google Tag Manager -->
		<script>
			(function(w, d, s, l, i) {
				w[l] = w[l] || [];
				w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
				var f = d.getElementsByTagName(s)[0],
					j = d.createElement(s),
					dl = l != 'dataLayer' ? '&l=' + l : '';
				j.async = true;
				j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
				f.parentNode.insertBefore(j, f);
			})(window, document, 'script', 'dataLayer', 'GTM-MGBHHHF');
		</script>
		<!-- End Google Tag Manager -->
		<style amp-boilerplate>
			body {
				-webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
				-moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
				-ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
				animation: -amp-start 8s steps(1, end) 0s 1 normal both;
			}
			@-webkit-keyframes -amp-start {
				from {
					visibility: hidden;
				}
				to {
					visibility: visible;
				}
			}
			@-moz-keyframes -amp-start {
				from {
					visibility: hidden;
				}
				to {
					visibility: visible;
				}
			}
			@-ms-keyframes -amp-start {
				from {
					visibility: hidden;
				}
				to {
					visibility: visible;
				}
			}
			@-o-keyframes -amp-start {
				from {
					visibility: hidden;
				}
				to {
					visibility: visible;
				}
			}
			@keyframes -amp-start {
				from {
					visibility: hidden;
				}
				to {
					visibility: visible;
				}
			}
		</style>
		<noscript>
			<style amp-boilerplate>
				body {
					-webkit-animation: none;
					-moz-animation: none;
					-ms-animation: none;
					animation: none;
				}
			</style>
		</noscript>
		<!-- END: STYLESHEET -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>IE9.js"></script>
			<style></style>
		<![endif]-->
	</head>
	<body>
		<!-- wrapper starts -->
		<div class="wrapper">
			<!-- calendario-content starts -->
			<div class="calendario-content content_contact vcard">
				<div class="calendario-data-main contacto_data_main" style="
    display: block;
    width: 100%;
">
					<div class="menu">&nbsp;</div>
					<h3 style="disp">
						<span
							><a href="javascript:history.back()"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></span
						>Regresar
					</h3>

					<div class="clear">&nbsp;</div>

					<div class="sidemenu">&nbsp;</div>
			
					<div class="clear">&nbsp;</div>
				</div>
				<div class="table-contact-main" style="
    width: 65%;
    display: block;
    margin: 0 auto;
">
					<div class="table-chefs-left table-contacto-left flt_lt directorio" style="width: 100%;text-align: center;">
						
						<h3><span
								><a href="javascript:history.back()"
									><i class="fa fa-chevron-left white" aria-hidden="true"></i></a></span
							>Tu mensaje ha sido enviado.</h3>
						
						<h2>En breve un representante de Table se pondrá en contacto.
</h2>
						
						
										
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
			<!-- calendario-content ends -->
			<!--footer starts-->
			<!--?php include 'footer.php'; ?-->
			<!--footer ends-->
		</div>
		<!-- wrapper ends -->
		<!-- SET: SCRIPTS -->
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.2.min.js"></script>
		<!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripting.js"></script> -->
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/customInput.jquery.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script>
			$(document).ready(function(e) {
				$('nav')
					.clone()
					.appendTo('.sidemenu');
				//menu-down
				$('.menu').click(function(e) {
					$('body').toggleClass('open-menu');
				});
				$('.foodandwines-links')
					.clone()
					.appendTo('.footer-logo');
				$('.foodandwine-cnt')
					.clone()
					.appendTo('.footer-logo');
				$('.foodandwine-data')
					.clone()
					.appendTo('.footer-bottom');
				$('.copy-right')
					.clone()
					.appendTo('.footer-bottom');
				// $('.horizontalvertical_scroll').mCustomScrollbar({
					axis: 'yx'
				});

				var ww = $(window).width();
				var limit = 991;
				function refresh() {
					ww = $(window).width();
					var w = ww < limit ? location.reload(true) : ww > limit ? location.reload(true) : (ww = limit);
				}

				var tOut;
				$(window).resize(function() {
					var resW = $(window).width();
					clearTimeout(tOut);
					if ((ww > limit && resW < limit) || (ww < limit && resW > limit)) {
						tOut = setTimeout(refresh, 100);
					}
				});
			});
		</script>
	</body>
</html>
