<!doctype html>
<html amp lang="en">
   <head>
      <meta charset="utf-8">
      <script async src="https://cdn.ampproject.org/v0.js"></script>
      <title>Table - Food and Wine México</title>
      <link rel="canonical" href="https://html1118.upupload.com/67102/tvkroguh67102/index.html"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="format-detection" content="telephone=no">
      <!-- SET: FAVICON -->
      <link rel="shortcut icon" type="image/x-icon" href="images/logo2.png">
      <!-- END: FAVICON -->

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <!-- SET: STYLESHEET -->
      <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/app.css">
      <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700" rel="stylesheet">

      <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/js/slick/slick.css"/>
      <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/js/slick/slick-theme.css"/>
      <script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>

      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
     
      <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
      <noscript>
      <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
      </noscript>
      <!-- END: STYLESHEET -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>IE9.js"></script>
      <style></style>
      <![endif]-->

      <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MGBHHHF');</script>
<!-- End Google Tag Manager -->
   </head>
   <body>
         <!-- banner starts -->
         <div class="banner">
         <video autoplay="" muted="" playsinline="" loop="" preload="" ><source src="<?php echo get_stylesheet_directory_uri(); ?>/video/Loop_Table_nuevo.mp4"></video>
         
            <div class="banner_content" id="home">            
               <a href="#">
                  <amp-img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-table-amex-wh.svg" width="530" height="300" alt="logo_1"></amp-img>
               </a>
               <div class="header" id="navbar" >
                 <input type="checkbox" id="btn-menu3" onchange="javascript:showContent()">
                 <label for="btn-menu3">
                  <i  class="fa fa-bars fa-2x" aria-hidden="true"></i>
                  <div class="menu-float" id="content">
                  <ul>
                     <li><a href="#home">Home</a></li>
                     <li><a href="#about">¿Qué es Table?</a></li>
                     <li><a href="#calendar">Experiencias</a></li>
                     <li><a href="#chefs">Table Chefs</a></li>
                     <li><a href="#sede">Hotel Sede</a></li>
                     <li><a href="#directorio">Contactos</a></li>
                  </ul>
                  <p class="text-menu thin">&copy; <?php
  $fromYear = 2018;
  $thisYear = (int)date('Y');
  echo $fromYear . (($fromYear != $thisYear) ? '-' . $thisYear : '');?> Food & Wine México.
                  </p>
                  </div>
                  </label>
                 
                <nav class="menu2">
                  <ul >
                     <!-- <li><a href="#home">Home</a></li> -->
                     <li><a href="#about">¿Qué es Table?</a></li>
                     <li><a href="#calendar">Experiencias</a></li>
                     <li><a href="#chefs">Table Chefs</a></li>
                     <li><a href="#sede">Hotel Sede</a></li>

                     <li><a href="#directorio">Contactos</a></li>
                  </ul>
               </nav>
         </div>
            </div>
         </div>
